# Gitlab Terraform with Azure CLI

## Information and Usage
In order to use GitLab managed Terraform state, it's recommended to use the helper image provided by GitLab.

The Dockerfile for the helper image has an argument 'BASE_IMAGE', which sets the FROM directive and can be used to add GitLab managed Terraform state to existing images.

This repo simply builds the helper image with the official azure-cli image as the BASE_IMAGE, generating an image with support for both.

## Usage
See official documentation for best usage.
https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html


## Sources
 - [gitlab-terraform docker image](https://gitlab.com/gitlab-org/terraform-images/)
 - [azure-cli docker image](https://github.com/Azure/azure-cli)