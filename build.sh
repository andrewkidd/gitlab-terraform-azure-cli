#!/bin/sh
set -e

# vars
IMAGE_NAME=$CI_PROJECT_NAME
TERRAFORM_BINARY_VERSION=1.1.2
BASE_IMAGE=mcr.microsoft.com/azure-cli
SOURCE_REPO=https://gitlab.com/gitlab-org/terraform-images.git

# build
docker image build \
    --progress plain \
    --build-arg TARGETARCH=amd64 \
    --build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION \
    --build-arg BASE_IMAGE=$BASE_IMAGE \
    -t $CI_REGISTRY_IMAGE/$IMAGE_NAME:${CI_COMMIT_SHORT_SHA} \
    --cache-from $CI_REGISTRY_IMAGE/$IMAGE_NAME:${CI_COMMIT_SHORT_SHA} \
    $SOURCE_REPO